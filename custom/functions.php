<?php
/**
 * Functions.php
 *
 * @package  Theme_Customisations
 * @author   WooThemes, Shailesh Shrestha
 * @since    1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * functions.php
 * Add PHP snippets here
 */

// Adding Google Analytics Snippet
/**
add_action( 'wp_head', function() { ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115624530-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-115624530-1');
    </script>

<?php } );

/*
if (!function_exists("parent_function_name")) {
    parent_function_name() {
           ...
      }
}
*/


add_action('init', 'boniyas_remove_stuffs');
function boniyas_remove_stuffs(){
	add_filter('is_active_sidebar', '__return_false');

	remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
	remove_action('woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10);
	remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
	remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10);
	remove_action('woocommerce_before_shop_loop', 'storefront_woocommerce_pagination', 30);
    remove_action('woocommerce_before_main_content','woocommerce_breadcrumb',10);

	remove_action( 'storefront_content_top','woocommerce_breadcrumb',10 );
	// for storefront
    remove_action('storefront_before_content','woocommerce_breadcrumb', 10);
	remove_action( 'storefront_footer', 'storefront_credit', 20);
}

/*
 * Hide category product count in product archives
 */
add_filter ('woocommerce_subcategory_count_html', '__return_false');

// Editing storefront Credit
if ( ! function_exists( 'storefront_credit' ) ) {
	/**
	 * Display the theme credit
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_credit() {
		?>
        <div class="site-info">

			<?php echo 'Change this Text From Child Theme function.php'
			/**esc_html( apply_filters( 'storefront_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . date( 'Y' ) ) ); ?>
			<?php if ( apply_filters( 'storefront_credit_link', true ) ) { ?>
			<br /> <?php echo '<a href="https://woocommerce.com" target="_blank" title="' . esc_attr__( 'WooCommerce - The Best eCommerce Platform for WordPress', 'storefront' ) . '" rel="author">' . esc_html__( 'Built with Storefront &amp; WooCommerce', 'storefront' ) . '</a>' ?>
			<?php } **/ ?>

        </div><!-- .site-info -->
		<?php
	}
}

if ( ! function_exists( 'storefront_page_header' ) ) {
	/**
	 * Display the page header with background Image
	 *
	 * @since 1.0.0
	 */
	function storefront_page_header(){

		if(is_product_category() ){
		    global $wp_query;
		    $cat = $wp_query->get_queried_object();
		    $featured_image = wp_get_attachment_url(get_woocommerce_term_meta($cat->term_id,'thumbnail_id',true));
        }
        else {
	        $featured_image = get_the_post_thumbnail_url( get_the_ID(), 'full' );
        }
		?>
        <div class="full-width" style="<?php boniyas_featured_image_style(); ?>" data-featured-image="<?php echo $featured_image; ?>">
            <header class="entry-header" >
				<?php
				the_title( '<h1 class="entry-title">', '</h1>' );
				?>
            </header><!-- .entry-header -->

        </div>
		<?php
	}
}

add_filter ('storefront_homepage_content_styles', 'boniyas_homepage_content_style');
if (!function_exists('boniyas_homepage_content_style')){
    function boniyas_homepage_content_style(){
	    $featured_image   = get_the_post_thumbnail_url( get_the_ID() );
	    $background_image = '';
	    $style            = 'linear-gradient( rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.75)) ';

	    if ( $featured_image ) {
		    $background_image = $style.', url(' . esc_url( $featured_image ) . ')';
	    }

	    $styles = array();

	    if ( '' !== $background_image ) {
		    $styles['background-image'] = $background_image;
	    }

        $styles ['background-size'] = 'cover';
        $styles ['background-position'] = 'center center';
	    //$styles['width']= '100vw';
	    //$styles['position'] = 'relative';
	    //$styles['left'] = '50%';
	    //$styles['right'] = '50%';
	    $styles['margin-top'] = '0';
	    //$styles['margin-right'] = '-50vw';
	    //$styles['margin-bottom'] = '1.68em';
	    //$styles['margin-left'] = '-50vw';
	    $styles['transition'] = 'all .5s ease-in-out';

        return $styles;
    }
}

if (! function_exists( 'boniyas_featured_image_style')) {
	/**
	 * Displays a full width background with inline styles
	 *
	 * @since 1.0.8
	 * @param $id
	 * @see storefront_homepage_content_styles()
	 */
	function boniyas_featured_image_style( $id ="") {
		if($id == get_option('woocommerce_shop_page_id')){
			$featured_image = get_the_post_thumbnail_url( $id, 'full');
		}
		elseif ($id !== "" ){
			if(is_product_category() ){
				global $wp_query;
				$cat = $wp_query->get_queried_object();
				$featured_image = wp_get_attachment_url(get_woocommerce_term_meta($cat->term_id,'thumbnail_id',true));
			}
        }
        else {
			$featured_image = get_the_post_thumbnail_url( get_the_ID(), 'full' );
		}
		$background_image = '';
		$style            = 'linear-gradient( rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.75)) ';

		if ( $featured_image ) {
			$background_image = $style.', url(' . esc_url( $featured_image ) . ')';
		}

		$styles = array();

		if ( '' !== $background_image ) {
			$styles['background-image'] = $background_image;
		}else{
			$styles['background-image'] = $style;
			$styles['background-color'] = "#6b5b95";
		}

		foreach ( $styles as $style => $value ) {
			echo esc_attr( $style . ': ' . $value . '; ' );
		}
		return $featured_image;
	}
}
// Rearrange the Header elements

add_action( 'init', 'rearrange_header', 15);
function rearrange_header() {
	/**
	 * Functions hooked into storefront_header action BEFORE
	 *
	 * @hooked storefront_skip_links                       - 0
	 * @hooked storefront_social_icons                     - 10
	 * @hooked storefront_site_branding                    - 20
	 * @hooked storefront_secondary_navigation             - 30 => Change to   - 0
	 * @hooked storefront_product_search                   - 40 => Changed to  - 30
	 * @hooked storefront_primary_navigation_wrapper       - 42
	 * @hooked storefront_primary_navigation               - 50
	 * @hooked storefront_header_cart                      - 60
	 * @hooked storefront_primary_navigation_wrapper_close - 68
	 *
	 * @hooked on_sale_menu                                - 10
	 */
	remove_action( 'storefront_header', 'storefront_secondary_navigation',             30 );
	add_action( 'storefront_header', 'storefront_secondary_navigation',             0 );
	remove_action( 'storefront_header', 'storefront_product_search', 40);
	add_action('storefront_header', 'storefront_product_search',    30);
	remove_action( 'storefront_header', 'storefront_header_cart', 60);
	// remove_action('storefront_header', 'storefront_primary_navigation', 50);
	// remove_action('storefront_header', 'storefront_primary_navigation_wrapper', 42);
	// remove_action('storefront_header', 'storefront_primary_navigation_wrapper_close', 68);

	add_action('storefront_header', 'boniyas_main_menu', 20);
}
add_action( 'init', 'theme_customization_boniyas_register_nav');
/**
 * This Customization adds new theme location to the website
 *
 * @see main_menu()
 * @return void
 */
function theme_customization_boniyas_register_nav(){
	register_nav_menu('boniyas-main-menu', __('Main Menu' ));
}

// Function to add new menu location
function boniyas_main_menu() {
	?>
    <nav class="boniyas-main-navigation" role="navigation" >
		<?php
		if (has_nav_menu('boniyas-main-menu')) {
			wp_nav_menu(
				array(
					'theme_location' => 'boniyas-main-menu',
					'fallback_cb'    => '',
				)
			);
		}
		?>
    </nav><!-- #site-navigation -->
	<?php
}
/*
 * Remove Page Titles of Woocommerce
 */
// add_filter( 'woocommerce_show_page_title', '__return_false' );

/*
 *  Storefront Homepage template edit
 * @see storefront/template-homepage.php
 */
add_action('homepage', 'boniyas_homepage');
function boniyas_homepage(){
	/**
	 * Functions hooked in to homepage action
	 *
	 * @hooked storefront_homepage_content      - 10
	 * @hooked storefront_product_categories    - 20
	 * @hooked storefront_recent_products       - 30
	 * @hooked storefront_featured_products     - 40
	 * @hooked storefront_popular_products      - 50
	 * @hooked storefront_on_sale_products      - 60
	 * @hooked storefront_best_selling_products - 70
	 */
    remove_action('homepage', 'storefront_product_categories', 20);
	remove_action('homepage', 'storefront_popular_products', 50);
	remove_action('homepage', 'storefront_on_sale_products', 60);
	remove_action('homepage', 'storefront_best_selling_products', 70);
	add_action('homepage', 'boniyas_contact_form', 20);
}


if ( ! function_exists( 'boniyas_contact_form' ) ) {
	/**
	 * Simple form
	 *
	 * @since 2.3.0
	 */
	function boniyas_contact_form() {
		if ( WC()->cart->get_cart_contents_count !== 0 ) {
			?>
            <div class="single-product-form">
				<?php echo do_shortcode( '[woocommerce_checkout]' ); ?>
            </div>
		<?php }
	}
}


/*
 * To change Headings of Storefront Generated Products Lists
 */
add_filter('storefront_product_categories_args', 'boniyas_product_category_args');
function boniyas_product_category_args() {
	$args = array(
		'limit' 			=> 4,
		'columns' 			=> 4,
		'child_categories' 	=> 0,
		'orderby' 			=> 'name',
		'title'				=> __( 'We Provide', 'storefront' ),
	) ;
	return $args;
}
add_filter('storefront_recent_products_args', 'boniyas_recent_products_args');
function boniyas_recent_products_args() {
	$args ['title'] = __( 'Our New Ventures', 'storefront' );
	return $args;
}
add_filter('storefront_featured_products_args', 'boniyas_featured_products_args');
function boniyas_featured_products_args() {
	$args ['title'] =__( 'Our Recommended Services', 'storefront' );
	return $args;
}
add_filter('storefront_popular_products_args', 'boniyas_popular_products_args');
function boniyas_popular_products_args(){
	$args ['title'] =__( 'Our Customer Recommended Services', 'storefront');
	return $args;
}
add_filter('storefront_best_selling_products_args', 'boniyas_best_selling_products_args');
function boniyas_best_selling_products_args(){
	$args ['title']= __( 'Our Customer Admired Services', 'storefront');
	return $args;
}
add_filter('loop_shop_per_page', 'boniyas_loop_shop_per_page');
function boniyas_loop_shop_per_page(){
    return 6;
}
add_filter('storefront_loop_columns', 'boniyas_loop_columns');
add_filter('loop_shop_columns', 'boniyas_loop_columns');
function boniyas_loop_columns(){
	return 1;
}
add_filter('storefront_related_products_args','boniyas_default_columns_per_page');
add_filter('storefront_recent_products_args','boniyas_default_columns_per_page');
add_filter('storefront_best_selling_products_args','boniyas_default_columns_per_page');
add_filter('storefront_best_selling_products_args','boniyas_default_columns_per_page');
add_filter('storefront_on_sale_products_args','boniyas_default_columns_per_page');

function boniyas_default_columns_per_page($args){
	$args['limit']   = 6;
	$args['columns'] = 1;
	return $args;
}


/*
 * To change how add to cart button functions
 * @return $button
 */


add_filter( 'woocommerce_loop_add_to_cart_link', 'boniyas_loop_add_to_cart_button', 30, 2 );
function boniyas_loop_add_to_cart_button( $button, $product  ) {
	if( $product->is_type( 'simple' ) ){
		$button_text = __( "View Service", "woocommerce" );
		$button = '<a class="button" href="' . $product->get_permalink() . '">' . $button_text . '</a>';
	}
	return $button;
}

add_action('template_redirect', 'add_product_to_cart');
function add_product_to_cart(){
	if( is_product()){
		$product_id = get_the_ID();
		WC()->cart->empty_cart();
		WC()->cart->add_to_cart( $product_id );
	}
}

add_filter( 'woocommerce_product_add_to_cart_text', 'boniyas_cart_button_text' );
add_filter( 'woocommerce_product_single_add_to_cart_text', 'boniyas_cart_button_text' );

function boniyas_cart_button_text() {
	return __( 'Add to Wishlist', 'woocommerce' );
}

add_filter( 'woocommerce_order_button_text', 'boniyas_order_button_text' ) ;
/**
 * @see woocommerce_template_single_add_to_cart()
 * @return string
 */
function boniyas_order_button_text(){
	if(is_product()){
		return __('Send', 'woocommerce');
	}
    elseif (is_checkout()){
		return __('Send', 'woocommerce');
	}
	else {
		return __('Submit', 'woocomeerce');
	}
}

add_filter('woocommerce_product_tabs', 'boniyas_default_product_tabs',98);
/**
 * Add default product tabs to product pages.
 *
 * @param array $tabs Array of tabs.
 * @return array
 */
function boniyas_default_product_tabs( $tabs = array() ) {

	// Description tab - shows product content.

    $tabs['description'] = array(
			'title'    => __( 'Description', 'woocommerce' ),
			'priority' => 10,
			'callback' => 'woocommerce_product_description_tab',
		);
    $tabs['additional_information'] = array(
			'title'    => __( 'Additional information', 'woocommerce' ),
			'priority' => 20,
			'callback' => 'woocommerce_product_additional_information_tab',
		);
	$tabs['reviews'] = array(
			/* translators: %s: reviews count */
			'title'    => __( 'References', 'woocommerce' ),
			'priority' => 30,
			'callback' => 'comments_template',
		);

	return $tabs;
}

/*
 * Change button labels
 */

add_filter( 'woocommerce_my_account_get_addresses', 'boniyas_my_account_get_addresses', 1, 1);
/**
 * Changes Billing Address to My Address and Removes Shipping Address
 *
 * @return array
 */
function boniyas_my_account_get_addresses (){
	$address = array(
		//'billing' => __( 'My address', 'woocommerce' ),
		'shipping' => __( 'My address', 'woocommerce' ),
	);
	return $address;
}

/**
 * Change Order label of orders to Request
 * Remove payment-methods and downloads
 * @see wc_get_account_menu_items()
 */
add_filter( 'woocommerce_account_menu_items', 'boniyas_account_menu_items');
function boniyas_account_menu_items() {
	$boniyas_account_menu_items = array(
		'dashboard'       => __( 'Dashboard', 'woocommerce' ),
		'orders'          => __( 'Requests', 'woocommerce' ),
		// 'downloads'       => __( 'Downloads', 'woocommerce' ),
		'edit-address'    => __( 'Addresses', 'woocommerce' ),
		// 'payment-methods' => __( 'Payment methods', 'woocommerce' ),
		'edit-account'    => __( 'Account details', 'woocommerce' ),
		'customer-logout' => __( 'Logout', 'woocommerce' ),
	);
	return $boniyas_account_menu_items;
}


/**
 * @see wc_get_account_orders_columns()
 * @return array
 */
add_filter( 'woocommerce_account_orders_columns', 'boniyas_account_orders_columns');
function boniyas_account_orders_columns() {
	$columns =  array(
		'order-number'  => __( 'Requests Number', 'woocommerce' ),
		'order-date'    => __( 'Date', 'woocommerce' ),
		'order-status'  => __( 'Status', 'woocommerce' ),
		// 'order-actions' => __( 'Actions', 'woocommerce' ),
	);
	return $columns;
}

add_filter ('woocommerce_default_address_fields', 'boniyas_default_address_fields');
function boniyas_default_address_fields($address_fields) {
	$address_fields['address_1']['required'] = false;
	$address_fields['address_2']['required'] = false;
	$address_fields['city']['required'] = false;
	$address_fields['state']['required'] = false;
	$address_fields['postcode']['required'] = false;
	$address_fields['country']['required'] = false;
	return $address_fields;
}
add_filter( 'woocommerce_checkout_fields', 'boniyas_checkout_fields');

/**
 * @param $fields
 *
 * @return mixed
 *
 * @see woocommerce_form_field()
 */

function boniyas_checkout_fields($fields){
	unset( $fields['billing']['billing_country']);
	unset( $fields['billing']['billing_address_1']);
	unset( $fields['billing']['billing_address_2']);
	unset( $fields['billing']['billing_city']);
	unset( $fields['billing']['billing_state']);
	unset( $fields['billing']['billing_postcode']);
	unset( $fields['billing']['billing_first_name']);
	unset( $fields['billing']['billing_last_name']);
	$fields['billing']['billing_company'] = array(
		'label'         => __('Identity', 'woocommerce'),
		'placeholder'   => _x('Name / Company Name. - Optional', 'placeholder', 'woocommerce'),
		'required'      => false,
		'class'         => array( 'form-row-wide'),
		'clear'         => true,
	);
	$fields['billing']['billing_phone']  = array(
		'label'         => __('Contact Number', 'woocommerce'),
		'placeholder'   => _x('xx-xxxx-xxxx - * Required', 'placeholder', 'woocommerce'),
		'required'      => true,
		'class'         => array( 'form-row-wide'),
		'clear'         => true,
	);
	$fields['billing']['billing_email']  = array(
		'label'         => __('Email', 'woocommerce'),
		'placeholder'   => _x('email@address.com - * Required', 'placeholder', 'woocommerce'),
		'required'      => true,
		'class'         => array( 'form-row-wide'),
		'clear'         => true,
	);

	$fields['shipping']['shipping_first_name'] = array(
		'label'     => __('First name', 'woocommerce'),
		'placeholder'   => _x('','placeholder','woocommerce'),
		'required'      => false,
		'class'         => array('from-row-first'),
		'clear'         =>true,
	);

	$fields['shipping']['shipping_last_name'] = array(
		'label'     => __('Last name', 'woocommerce'),
		'placeholder'   => _x('','placeholder','woocommerce'),
		'required'      => false,
		'class'         => array('from-row-last'),
		'clear'         =>true,
	);

	$fields['shipping']['shipping_company'] = array(
		'label'         => __('Subject', 'woocommerce'),
		'placeholder'   => _x('','placeholder','woocommerce'),
		'required'      => false,
		'class'         => array('from-row-wide'),
		'clear'         =>true,
	);

	$fields['order']['order_comments'] = array(
		'type'      => 'textarea',
		'label'     => __('Cover Letter'),
		'maxlength'     => '2000',
		'placeholder'   => _x('Describe why we should hire you. Don`t forget to mention what kind(s) of job you are applying to and when you can start. ( ! The length is limited to 2000 characters.)', 'placeholder', 'woocommerce'),
		'required'      => false,
		'class'         => array( 'form-row notes'),
		'clear'         => true,
	);
	return $fields;

}

/**
 * Redesign Single Product Page
 */

add_action('woocommerce_before_single_product', 'boniyas_single_product_page');

function boniyas_single_product_page(){
	/**
	 * @hooked woocommerce_template_single_title - 5
	 * @hooked woocommerce_template_single_rating - 10
	 * @hooked woocommerce_template_single_price - 10
	 * @hooked woocommerce_template_single_excerpt - 20
	 * @hooked woocommerce_template_single_add_to_cart - 30
	 * @hooked woocommerce_template_single_meta - 40
	 * @hooked woocommerce_template_single_sharing - 50
	 * @hooked WC_Structured_Data::generate_product_data() - 60
	 */
	remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
	remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

	add_action('woocommerce_before_single_product_summary','woocommerce_template_single_title', 5);
	add_action('woocommerce_before_single_product_summary', 'woocommerce_template_single_excerpt', 20);

}

add_action('woocommerce_before_shop_loop_item', 'boniyas_content_product');

function boniyas_content_product() {
	remove_all_actions('woocommerce_after_shop_loop_item_title');
	add_action('woocommerce_after_shop_loop_item_title','woocommerce_template_single_excerpt', 10);
}

add_filter( 'storefront_handheld_footer_bar_links', 'boniyas_handheld_footer_bar_link' );
function boniyas_handheld_footer_bar_link( $links ) {
	unset( $links['cart'] );
	$new_links = array(
		'home' => array(
			'priority' => 10,
			'callback' => 'boniyas_home_link',
		),
		'request' => array(
			'priority' => 30,
			'callback' =>'boniyas_checkout_link'
		)
	);

	$links = array_merge( $new_links, $links );

	return $links;
}

function boniyas_home_link() {
	echo '<a href="' . esc_url( home_url( '/' ) ) . '">"' . __( 'Home' ) . '</a>';
}

function boniyas_checkout_link() {
	echo '<a href="' . esc_url( wc_get_checkout_url()).'">"'. __( 'Request') . '</a>';
}

add_filter ('the_title', 'boniyas_page_titles', 10, 2);

function boniyas_page_titles($title,$id) {
	if (is_wc_endpoint_url('orders') && in_the_loop()) {
		$title = 'Requests';
	}if (is_wc_endpoint_url('order-received')&& in_the_loop()) {
		$title = 'Request Received';
	}
	return $title;
}

add_action( 'template_redirect', 'boniyas_add_general_inquiry_to_cart');
/*
 * Add General inquiry product hardcoded
 * @todo Find a way to remove hardcoded adding of general inquiry
 */
function boniyas_add_general_inquiry_to_cart() {
	if ( ! is_admin() && is_checkout() && WC()->cart->get_cart_contents_count() < 1){
		$general_inquiry_id = 65;
		WC()->cart->add_to_cart( $general_inquiry_id );
	}
}

add_filter('woocommerce_ajax_loader_url', 'boniyas_ajax_loader_url');
function boniyas_ajax_loader_url() {
	if(is_checkout() || is_product()) {
		return __( get_template_directory_uri() . '/image/loader-ajax-new.gif', 'woocommerce' );
	}
	else{
		return __(get_template_directory_uri() . '/image/loader-ajax-new.gif', 'woocommerce' );
	}
}


if ( ! function_exists( 'storefront_sticky_single_add_to_cart' ) ) {
	/**
	 * Sticky Add to Cart
	 *
	 * @since 2.3.0
	 */
	function storefront_sticky_single_add_to_cart() {
		global $product;

		if ( class_exists( 'Storefront_Sticky_Add_to_Cart' ) || true !== get_theme_mod( 'storefront_sticky_add_to_cart' ) ) {
			return;
		}

		if ( ! is_product() ) {
			return;
		}

		$params = apply_filters( 'storefront_sticky_add_to_cart_params', array(
			'trigger_class' => 'entry-summary'
		) );

		wp_localize_script( 'storefront-sticky-add-to-cart', 'storefront_sticky_add_to_cart_params', $params );

		wp_enqueue_script( 'storefront-sticky-add-to-cart' );
		?>
        <section class="storefront-sticky-add-to-cart">
            <div class="col-full">
                <div class="storefront-sticky-add-to-cart__content">
					<?php echo wp_kses_post( woocommerce_get_product_thumbnail() ); ?>
                    <div class="storefront-sticky-add-to-cart__content-product-info">
                        <span class="storefront-sticky-add-to-cart__content-title"><?php esc_attr_e( 'If you\'re interested in', 'storefront' ); ?> <strong><?php the_title(); ?></strong></span>
                        <span class="storefront-sticky-add-to-cart__content-boniyas"><?php esc_attr_e( 'Just send us your contact', 'storefront'); ?><?php esc_attr_e( 'And we shall be in touch.' ); ?></span>
                    </div>
                    <div class="storefront-sticky-add-to-cart__content-boniyas-checkout">
	                    <?php echo do_shortcode('[woocommerce_checkout]'); ?>
                    </div>
                </div>
            </div>
        </section><!-- .storefront-sticky-add-to-cart -->
		<?php
	}
}

